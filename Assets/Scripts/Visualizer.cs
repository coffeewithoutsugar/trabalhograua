﻿using Assets.Patterns;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
	[RequireComponent(typeof(AudioSource))]
	public class Visualizer : MonoBehaviour
	{
		#region Fields
		public PrefabBehaviour prefab;
		public PatternType type;
		public int maxHeight;
		public int size; //Used as radius for Circle and maxPoints for Lorenz
		public float responseSpeed;
		public static Color one, two;
		public static List<PrefabBehaviour> objects = new List<PrefabBehaviour>();
		#endregion

		#region Properties
		/// <summary>
		/// Folder to organize prefabs under
		/// </summary>
		private GameObject folder { get; set; }
		#endregion

		#region Event Functions
		/// <summary>
		/// Creates prefabs in the given pattern
		/// </summary>
		void Start()
		{

			two = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
			one = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

			//Create Pattern using PatternFactory class
			Pattern pattern = PatternFactory.CreatePattern(type, size); //type selected from dropdown in Unity editor

			//Create folder to file each prefab in
			folder = new GameObject(type.ToString() + " Prefabs (" + pattern.Count + ")");
			folder.transform.SetParent(transform);

			//Generate a prefab facing up and down at each point
			foreach (Vector3 point in pattern)
			{
				GeneratePrefab(point + point * 2f, maxHeight);
				//GeneratePrefab(point + point * 2f, -1 * maxHeight);
			}
			transform.eulerAngles = Vector3.zero;
			transform.rotation = Quaternion.identity;
		}

		private void ChangeColors()
		{
			one = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
			two = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
			foreach (var item in objects)
			{
				item.ChangeColor();
			}
			int count = Random.Range(4, 9);
			int index = 0;

			for (int i = 0; i < count; i++)
			{
				index = Random.Range(0, objects.Count);

				ParticleSystem.MainModule settings = objects[index].particle.main;

				if (i % 2 == 0)
					settings.startColor = new ParticleSystem.MinMaxGradient(one);
				else
					settings.startColor = new ParticleSystem.MinMaxGradient(two);
				objects[index].particle.Play();
			}
		}

		/// <summary>
		/// Close application if escape key is pressed and change the color of the cubes.
		/// </summary>
		void Update()
		{
			if(AudioManager.GetSpectrumValue(0) >= .1f || Input.GetKeyDown(KeyCode.Space))
			{
				ChangeColors();
			}
			Debug.Log(AudioManager.GetSpectrumValue(0));
			if (Input.GetKey("escape"))
			{
				Application.Quit();
			}
		}
		#endregion

		#region Methods
		/// <summary>
		/// Called for each vector created in Start() to create a new prefab object in that location
		/// </summary>
		/// <param name="vector"></param>
		private void GeneratePrefab(Vector3 vector, int height)
		{
			PrefabBehaviour prefab = (PrefabBehaviour)Instantiate(this.prefab, vector, this.prefab.transform.rotation);

			//sets up height, location, and color
			prefab.SetupPrefab(height, vector.magnitude, responseSpeed);

			//file new prefab in folder
			prefab.transform.SetParent(folder.transform);

			objects.Add(prefab);

		}
		#endregion
	}
}