﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeAudios : MonoBehaviour
{
	public List<AudioClip> audioClips;

	private int count;

	public AudioSource audioSource;
	// Start is called before the first frame update
	void Start()
	{
		count = 0;
		audioSource.clip = audioClips[count];
		audioSource.Play();
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			count++;
			count = (count + audioClips.Count) % audioClips.Count;
			audioSource.Pause();
				audioSource.clip = audioClips[count];
				audioSource.Play();
		}
		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			count--;
			count = (count + audioClips.Count) % audioClips.Count;
			audioSource.Pause();
			audioSource.clip = audioClips[count];
			audioSource.Play();
		}

	}
}
