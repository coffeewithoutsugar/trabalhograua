﻿using UnityEngine;

namespace Assets.Scripts
{
	/// <summary>
	/// Behaviors for each prefab like update it's size and color to audio
	/// </summary>
	public class PrefabBehaviour : MonoBehaviour
	{
		#region Fields
		public ParticleSystem particle;
		private Vector3 startScale;
		private Color startColor;
		private MeshFilter startMesh;
		private Renderer rend;
		[SerializeField]
		private float length;

		#endregion

		#region Properties
		internal float responseSpeed;
		internal float maxHeight { get; set; }
		[NaughtyAttributes.ShowNativeProperty]
		internal int spectrumIndex { get; set; }
		#endregion

		#region Event Functions

		public void ChangeColor()
		{
			startColor = Color.Lerp(Visualizer.one, Visualizer.two, length % 1);
		}

		/// <summary>
		/// Saves starting state to properties
		/// </summary>
		private void Start()
		{
			startScale = transform.localScale;
			startMesh = GetComponent<MeshFilter>();
		}

		/// <summary>
		/// Updates prefab's height or size
		/// </summary>
		private void Update()
		{
			//scale current spectrum window value, given the maxHeight of prefab
			var desiredScale = 1 + AudioManager.GetSpectrumValue(spectrumIndex) * maxHeight;

			if (startMesh.name.Contains("Cylinder"))
			{
				UpdateCylinderHeight(desiredScale);
			}
			else if (startMesh.name.Contains("Cube"))
			{
				UpdateCubeHeight(desiredScale);
			}
			else
			{
				UpdateSize(desiredScale);
			}

			UpdateColor(desiredScale);
		}
		#endregion

		#region Methods
		/// <summary>
		/// Called in Visualizer/GeneratePrefab()
		/// </summary>
		internal void SetupPrefab(int height, float length, float responseSpeed)
		{
			maxHeight = height;
			this.length = length;
			//set location in audio spectrum window based on vector magnitude
			spectrumIndex = (int)(Mathf.Round(length));

			rend = GetComponent<Renderer>();
			startColor = Color.Lerp(Visualizer.one, Visualizer.two, length % 1);
			//set starting color based on vector magnitude
			rend.material.SetColor("_EmissionColor", startColor);
			rend.material.color = startColor;

			//set latency for color and height updates
			this.responseSpeed = responseSpeed;
		}

		/// <summary>
		/// Update height for cubes in z dimension
		/// </summary>
		private void UpdateCubeHeight(float scale)
		{
			//update current height to height in AudioManager
			startScale.z = Mathf.Lerp(transform.localScale.z, scale, Time.deltaTime * responseSpeed);
			transform.localScale = startScale;
		}

		/// <summary>
		/// Update height for cylinders in y dimension
		/// </summary>
		private void UpdateCylinderHeight(float scale)
		{
			//update current height to height in AudioManager
			startScale.y = Mathf.Lerp(transform.localScale.y, scale, Time.deltaTime * responseSpeed);
			transform.localScale = startScale;
		}

		/// <summary>
		/// Update size in all 3 dimensions
		/// </summary>
		private void UpdateSize(float scale)
		{
			transform.localScale = new Vector3(scale + startScale.x, scale + startScale.y, scale + startScale.z);
		}

		/// <summary>
		/// Interpolate color from starting color to shade of blue
		/// </summary>
		private void UpdateColor(float scale)
		{
			//Color newColor = Color.Lerp(startColor, startColor * Mathf.Abs(scale), Time.deltaTime);
			rend.material.SetColor("_EmissionColor", startColor * Mathf.Lerp(transform.localScale.y, scale, Time.deltaTime * responseSpeed));
			rend.material.color = startColor;

		}
		#endregion
	}
}


