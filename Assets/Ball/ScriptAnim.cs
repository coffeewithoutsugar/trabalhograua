﻿using EZCameraShake;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;
using Deform.Masking;

public class ScriptAnim : MonoBehaviour
{
	public bool isBall = false;

	public Image mask;

	public Camera mainCamera;

	public float magn = 1, rough = .5f, fadeIn = 0.8f, fadeOut = 1f;

	public Gradient gradient;

	public PostProcessVolume[] volumes;

	public float lensDistortionValue = 0f;

	private Animator animator;

	private ParticleSystem[] particles;

	private MeshRenderer meshRenderer;

	private List<LensDistortion> lensDistortion = new List<LensDistortion>();

	private int currentColor = 0;

	public static ScriptAnim[] scriptAnim;

	private bool sense = false, changeColor = false;

	private float timer, time;

	public AnimationCurve colorAnimation;

	private AudioSource ballSound;

	public AudioSource clickSound;

	public Deform.TwistDeformer twistDeformer;

	public GameObject quitButton;

	private float startAngleDeform, endAngleDeform;

	public void Quit()
	{
		Application.Quit();
	}

	private void Start()
	{
		animator = GetComponent<Animator>();
		particles = GetComponentsInChildren<ParticleSystem>();
		meshRenderer = GetComponent<MeshRenderer>();
		ballSound = GetComponent<AudioSource>();
		if (scriptAnim == null)
		{
			scriptAnim = GameObject.FindObjectsOfType<ScriptAnim>();
		}

		LensDistortion distortion;

		foreach (PostProcessVolume volume in volumes)
		{
			distortion = null;

			volume.profile.TryGetSettings(out distortion);

			if (distortion != null)
			{
				lensDistortion.Add(distortion);
			}
		}

		startAngleDeform = twistDeformer.StartAngle;
		twistDeformer.StartAngle = 0f;
		endAngleDeform = twistDeformer.EndAngle;
		twistDeformer.EndAngle = 0f;
	}

	private void Update()
	{
		if (!isBall)
		{
			return;
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			quitButton.SetActive(!quitButton.activeSelf);
		}

		foreach (LensDistortion lens in lensDistortion)
		{
			lens.intensity.value = lensDistortionValue;
		}

		if (Input.GetMouseButtonDown(1))
		{
			RaycastHit hit;
			Ray ray = mainCamera.ScreenPointToRay(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y));

			if (Physics.Raycast(ray, out hit))
			{
				ScriptAnim anim = hit.transform.GetComponent<ScriptAnim>();
				anim?.OnMouseDown();
				Debug.Log(hit.transform.name, hit.transform);
			}
		}

		if (Input.GetMouseButtonDown(2))
		{
			sense = !sense;
		}

		if (Input.GetMouseButtonUp(1))
		{
			if (Time.timeScale == 1f)
			{
				if (isBall)
					ballSound.pitch = .5f;
				else
					clickSound.pitch = .5f;
				Time.timeScale = .25f;
			}
			else
			{
				if (isBall)
					ballSound.pitch = 1f;
				else
					clickSound.pitch = 1f;
				Time.timeScale = 1f;
			}
		}

		if (Input.GetKeyDown(KeyCode.Space))
		{
			foreach (var anim in scriptAnim)
			{
				anim.ChangeColor();
			}
		}

		if (sense)
		{
			timer += Time.deltaTime;
		}
		else
		{
			timer -= Time.deltaTime;
		}
		if (timer >= 1f)
		{
			timer = 1f;
		}
		else if (timer <= 0f)
		{
			timer = 0f;
		}
		mask.fillAmount = timer;
	}

	public void Sum()
	{
		int count = animator.GetInteger("move");
		if (count >= 2)
		{
			return;
		}
		animator.SetInteger("move", count + 1);
	}

	public void Sub()
	{
		int count = animator.GetInteger("move");
		if (count <= 0)
		{
			return;
		}
		animator.SetInteger("move", count - 1);
	}

	public void PlayParticles()
	{
		ballSound.volume = 0.5f;
		ballSound.Play();

		foreach (var item in particles)
		{
			item.Play();
		}
	}

	public void StopParticles()
	{
		foreach (var item in particles)
		{
			item.Stop();
		}
	}

	private void OnMouseDown()
	{
		if (isBall)
		{
			return;
		}
		ChangeColor();
		clickSound.volume = 0.2f;
		clickSound.Play();
	}

	private void ChangeColor()
	{
		int index = Random.Range(0, gradient.colorKeys.Length);
		if (index == currentColor)
		{
			ChangeColor();
		}
		else
		{
			changeColor = true;
			StartCoroutine(ChangeColorRoutine(index, .5f));
		}
	}

	public void Shake()
	{
		CameraShaker.Instance.ShakeOnce(magn, rough, fadeIn, fadeOut);
	}

	private IEnumerator ChangeColorRoutine(int index, float duration)
	{
		currentColor = index;
		float timer = 0f;
		Color endColor = gradient.colorKeys[index].color;
		while (timer < duration)
		{
			twistDeformer.StartAngle = Mathf.Lerp(0f, startAngleDeform, colorAnimation.Evaluate(timer / duration));
			twistDeformer.EndAngle = Mathf.Lerp(0f, endAngleDeform, colorAnimation.Evaluate(timer / duration));
			meshRenderer.material.SetColor("_EmissionColor", Color.Lerp(endColor, endColor * 1.7f, colorAnimation.Evaluate(timer / duration)));
			timer += Time.deltaTime;
			yield return null;
		}
		meshRenderer.material.SetColor("_EmissionColor", endColor);
		meshRenderer.material.color = endColor;
		changeColor = false;
	}
}
