﻿using StrangeIdeas.Util;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.Events;

public class Click : MonoBehaviour
{
	public ParticleSystem particles;

	public PostProcessVolume volume;

	public UnityEvent start;

	public void Start()
	{
		Time.timeScale = 0f;
	}

	void Update()
	{
		if (Input.GetMouseButtonUp(0))
		{
			Time.timeScale = 1f;
			UnityTaskManager.CreateTask(AninationDoF(.5f, 0.1f, 10f)).Start();
			enabled = false;
		}
	}

	public void LensDistortionEffect()
	{
		UnityTaskManager.CreateTask(AnimationLensDistortion(.5f, 0.1f, .25f)).Start();
	}

	private IEnumerator AnimationLensDistortion(float duration, float startValue, float endValue)
	{
		float timer = 0f;
		while (timer < duration)
		{
			timer += Time.deltaTime;
			yield return null;
		}
	}

	private IEnumerator AninationDoF(float duration, float startValue, float endValue)
	{
		float timer = 0f;

		DepthOfField dof = null;
		volume.profile.TryGetSettings(out dof);

		if (dof == null)
		{
			timer = duration;
		}

		while (timer < duration)
		{
			dof.aperture.value = Mathf.Lerp(startValue, endValue, timer / duration);
			timer += Time.deltaTime;
			yield return null;
		}

		dof.aperture.value = endValue;

		start?.Invoke();
	}

}
